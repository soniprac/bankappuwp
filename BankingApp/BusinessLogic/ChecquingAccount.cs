﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace BankApp.BusinessLogic
{
    /// <summary>
    /// Represents a checquing account that has an overdraft limit and a maximum interest rate
    /// </summary>
    class ChecquingAccount : Account
    {
        /// <summary>
        /// The amount of overdraft is constant. Defined as such and accessible
        /// through the name of the class along with the DOT notation
        /// </summary>
        private const double OVERDRAFT_LIMIT = 500.0;

        /// <summary>
        /// The maximum interest rate for checquing accounts. Defined as such and accessible
        /// through the name of the class along with the DOT notation 
        /// </summary>
        private const float MAX_INTEREST_RATE = 1.0f;

        /// <summary>
        /// The constructor assigns default values to each parameter allowing the code
        /// not to supply them (i.e. acct = ChequingAccount()). If the calling code does not supply
        /// values for the two parameters they will receive these default values. This is used
        /// when the accounts are created from data files
        /// </summary>
        /// <param name="acctNo">the account number that uniquely identified the account</param>
        /// <param name="acctHolderName">the name of the account holder</param>
        public ChecquingAccount(int acctNo = -1, string acctHolderName = ""):
            base(acctNo, acctHolderName)
        {
            //As the checquing account doesn't have any specific field variables there is nothing
            //to initialize. However the constructor is required in order to pass data provided
            //by client code to the base class through the base(..) call
        }

        /// <summary>
        /// The Annual Interest Rate property is overridden in order to verify that the annual interest rate is valid 
        /// for a checquing account when setting the interest rate
        /// </summary>
        public override float AnnualIntrRate
        {
            get
            {
                return base.AnnualIntrRate;
            }
            set
            {
                //check to ensure the annual interest rate is valid for a checquing account
                if (value > ChecquingAccount.MAX_INTEREST_RATE)
                {
                    //TODO: Replace this with exception
                    Debug.Assert(false, $"A checquing account cannot have an interest rate greater than {ChecquingAccount.MAX_INTEREST_RATE}");
                }
               
                //use the base class to set the annual interest rate
                base.AnnualIntrRate = value;
            }
        }

        /// <summary>
        /// Withdraw the given amount from the account and return the new balance. The checquing account override
        /// the base implementation in order to implement overdraft functionality that checquing accounts have. This
        /// allows clients to withdraw more than the balance, up to the overdraft limit amount
        /// </summary>
        /// <param name="amount">the amount to be withdrawn, cannot be negative or greater than balance and overdraft combined</param>
        /// <returns>the new account balance AFTER the amount was deposited to avoid a call to getBalance() if needed</returns>
        public override double Withdraw(double amount)
        {
            if (amount < 0)
            {
                //TODO: Replace this with exception
                Debug.Assert(false, "Invalid amount provided. Cannot withdraw a negative amount.");
            }

            //check the overdraft on top of the actual balance
            if (amount > _balance + ChecquingAccount.OVERDRAFT_LIMIT)
            {
                //TODO: Replace this with exception
                Debug.Assert(false, "Insufficient funds. Cannot withdraw the provided amount.");
            }
            //change the balance
            double oldBalance = _balance;
            _balance -= amount;
        
            //record the transaction
            _transactionList.Add(new Transaction(TransactionType.Withdrawal, amount, oldBalance, _balance));

            //provide the new balance to the caller to avoid a getBalance() call
            return _balance;
        }


    }
}
